package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

    static final Random rng = new Random();

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list);

        int lower_bound = 0;
        int upper_bound = listCopy.size() -1;


        final int middle_index = listCopy.size() / 2;

        while ( true ) {
            int suspected_median_index = randomPivotPartition(listCopy, lower_bound, upper_bound);

            if ( middle_index < suspected_median_index ) {

                upper_bound = suspected_median_index - 1;

            } else if ( suspected_median_index < middle_index ) {

                lower_bound = suspected_median_index + 1;

            } else { // Then the suspected_median_index must be the correct index.

                return listCopy.get( suspected_median_index );

            }
        }

    }

    /**
     * Randomly selects an element within the given lower- and upper-bound to be the pivot for the partition algorithm.
     * Then performs the partition algorithm and returns its output.
     * @param elements The list of elements to be modified
     * @param lower_bound The index of the lower bound of values that will be partitioned.
     * @param upper_bound The index of the upper bound of values that will be partitioned.
     * @return The index of the pivot after partitioning.
     */
    private <T extends Comparable<T>> int randomPivotPartition( List<T> elements, int lower_bound, int upper_bound ) {
        final int index_of_value_to_be_set_as_pivot = rng.nextInt(lower_bound, upper_bound + 1);
        swap(elements, upper_bound, index_of_value_to_be_set_as_pivot);
        return partition(elements, lower_bound, upper_bound);
    }

    /**
     * Swaps the elements at given indexes. Modifies given list.
     */
    private <T> void swap(List<T> list_to_modify, int index1, int index2) {
        final T value1 = list_to_modify.get(index1);
        final T value2 = list_to_modify.get(index2);

        list_to_modify.set(index1, value2);
        list_to_modify.set(index2, value1);
    }

    /**
     * Uses the value at the upper_bound as a pivot, and rearranges the given list of Ts so that every T lesser
     * than the pivot is to its left, and every T greater than the pivot is to its right.
     * @param elements The list of Ts to be reorganized around the pivot.
     * @param lower_bound The index of the starting point for what values we are interested in reorganizing.
     * @param upper_bound_and_pivot_index The index of the ending point for what values we are interested in
     *                                    reorganizing. This index will also be used as the pivot for convenience.
     * @return The index of the pivot after organizing the list.
     */
    private <T extends Comparable<T>> int partition(List<T> elements, int lower_bound, int upper_bound_and_pivot_index) {
        final T pivot_value = elements.get(upper_bound_and_pivot_index);

        int potential_pivot_index = lower_bound; // Is also the count of elements smaller than the pivot value when the lower bound is 0.

        for (int i = lower_bound; i < upper_bound_and_pivot_index; i++) {
            T current_value = elements.get(i);

            if (pivot_value.compareTo(current_value) > 0) { // Then the current_value is lesser than the pivot_value
                swap(elements, potential_pivot_index, i);
                potential_pivot_index++;
            }

        }

        swap(elements, upper_bound_and_pivot_index, potential_pivot_index);

        return potential_pivot_index;

    }


}
