package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {

        while ( true ) {
            boolean did_swap = false;

            for (int left_i = 0; left_i < list.size()-1; left_i++) {
                int right_i = left_i + 1;

                T left_element = list.get(left_i);
                T right_element = list.get(right_i);

                if ( left_element.compareTo(right_element) > 0 ) {
                    did_swap = true;
                    list.set(left_i, right_element);
                    list.set(right_i, left_element);
                }
            }

            if ( !did_swap ) {
                break;
            }
        }
    }
    
}
